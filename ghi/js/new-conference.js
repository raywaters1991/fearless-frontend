window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        console.log(data);
        const selectTag = document.getElementById('location');
        for (let name of data.conferences) {
            const option = document.createElement('option');
          // Set the '.value' property of the option element to the state's abbreviation
            option.value = conferences.name;
          // Set the '.innerHTML' property of the option element to the state's name
            option.innerHTML = conferences.name;
          // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
    };
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      console.log('need to submit the form data');
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      console.log(json);
      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
          },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newconference = await response.json();
        console.log(newconference);
      }
    });
  });
