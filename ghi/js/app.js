
function createCard(name, description, pictureUrl, starts, ends, locationName) {
    return `
      <div class="col">
        <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
            <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
            </div>
            <ul class="list-group list-group-flush">
            <li class="list-group-item">${starts} - ${ends}
            </div>
        </div>
      </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if (!response.ok) {
        throw new Error ('Response is not OK')

    } else {

        const data = await response.json();

    for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {

            const details = await detailResponse.json();
            const locationName = details.conference.location.name
            const starts = new Date(details.conference.starts).toLocaleDateString()
            const ends = new Date(details.conference.ends).toLocaleDateString()
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url
            const html = createCard(title, description, pictureUrl, starts, ends, locationName);
            const column = document.querySelector('.row');
            column.innerHTML += html;


        }
        }
    }
} catch (e) {
    const errorAlert = `
    <div class="alert alert-danger" role="alert">
        <strong>Error:</strong> ${e.message}
        </div>
        `;
        const container =document.querySelector('.error-container');
        container.innerHTML = errorAlert;

 }

});
